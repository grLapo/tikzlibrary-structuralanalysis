% Copyright 2019 Lapo Gori
% Author: Lapo Gori
%
% This file is part of tikzlibrarystructuralanalysis.
%
% tikzlibrarystructuralanalysis is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 3 of the License, or
% (at your option) any later version.
%
% tikzlibrarystructuralanalysis is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with tikzlibrarystructuralanalysis; if not, see <http:%www.gnu.org/licenses/>.

\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}

%Pages size
\usepackage{geometry}
\geometry{a4paper,left=3.0cm,right=3.0cm,top=3.0cm,bottom=2.0cm, headheight = 15pt}

\usepackage{graphicx} 
\usepackage{tikz}
\usepackage{pgfplots}
\pgfplotsset{compat=1.9}
\usetikzlibrary{structuralanalysis}

\usepackage{subfigure}

\usepackage{hyperref}
\usepackage{cleveref}
\usepackage{url}

%opening
\title{Tikz-Structural analysis: A Tikz library for structural analysis drawings}
\author{Lapo Gori}

\begin{document}
    
    \maketitle
    
    %     \begin{abstract}
    %         
    %     \end{abstract}
    
    \section{Introduction}
    The Tikz library \verb|structuralanalysis| is meant to be used to draw structural schemes like the one 
    depicted in \cref{fig:introduction-example}. 
    In its current version, it contains macros that allow to draw the following objects:
    \begin{itemize}
        \item restraints (roller restraints, pin restraints and fixed-end restraints)
        \item springs (linear springs)
        \item distributed loadings (orthogonal linear loadings and linear loadings in the y-direction)
    \end{itemize}
    which will be described in the following sections.
    
    \begin{figure}[htbp]
        \centering
        \begin{tikzpicture}
            
            %The bars
            \draw[line width = 1pt] (0.0, 0.0) --++ (0.0, 4.0) --++ (4.0, 0.0) --++ (4.0, -2.0) --++ (2.0, 0.0);
            
            %The restraints and spring
            \fixedendrestraint[line width = 1pt](0.0, 0.0, 0.0, 0.8);
            \pinrestraint[line width = 1pt](0.0, 4.0, -90.0, 0.4);
            \rollerrestraint[line width = 1pt](8.0, 2.0, 0.0, 0.4);
            \linearspring[line width = 1pt](10.0, 2.0, 180, 0.8, 0.2);
                        
            %The loadings
            \orthogonallinearload[line width = 0.5pt](0.0, 4.2, 2.5, 4.2, 0.25, 1.0, 0.5);
            \ylinearload[line width = 0.5pt, red](4.0, 4.2, 8.0, 2.2, 0.25, 1.0, 0.5);
            
        \end{tikzpicture}
        \caption{\label{fig:introduction-example}}
    \end{figure}
    
    \section{Objects}
    
    \subsection{Restraints}
    The available restraints, roller, pin and fixed-end, are depicted in \cref{fig:restraints}. 
    
    \begin{figure}[htbp]
        \centering
        \subfigure[Roller]{
            \begin{tikzpicture}
                \rollerrestraint[line width = 1pt](0.0, 0.0, 0.0, 1.0);
            \end{tikzpicture}
        }\qquad 
        \subfigure[Pin]{
            \begin{tikzpicture}
                \pinrestraint[line width = 1pt](0.0, 1.0, 0.0, 1.0);
            \end{tikzpicture}
        }\qquad 
        \subfigure[Internal pin]{
            \begin{tikzpicture}
                \internalpinrestraint[line width = 1pt](0.0, 1.0, 0.0, 1.0);
                \node[] at (0.0, -0.2) (ghostnode1) {};
            \end{tikzpicture}
        }\qquad 
        \subfigure[Fixed-end\label{fig:restraints-fixedend}]{
            \begin{tikzpicture}
                \draw[line width = 1pt] (0.0, -1.0) --++ (0.0, 1.0);
                \fixedendrestraint[line width = 1pt](0.0, -1.0, 0.0, 1.0);
                \node[] at (0.7, 0.0) (ghostnode) {};
                \node[] at (-0.7, 0.0) (ghostnode2) {};
            \end{tikzpicture}
        }
        \caption{Restraints\label{fig:restraints}}
    \end{figure}
    
    The roller restraint and the pin restraint are drawn with the following commands
    \begin{verbatim}
        \rollerrestraint[drawing options](x, y, angle, height)
        \pinrestraint[drawing options](x, y, angle, height)
    \end{verbatim}
    The field \verb|drawing options| may contains a line width or colour definition, \verb|x| and \verb|y| represent the coordinates of the top 
    vertex of the triangle, \verb|angle| defines the counterclockwise rotation with respect to the top 
    vertex of the triangle, and \verb|height| defines the height of the triangle. 
    The fixed-end restraint is instead drawn with the following command
    \begin{verbatim}
        \fixedendrestraint[drawing options](x, y, angle, width)
    \end{verbatim}
    The field \verb|drawing options| may contains a line width or colour definition, \verb|x| and \verb|y| represent the coordinates of the 
    point at which the restraint connect to the vertical bar in \cref{fig:restraints-fixedend}, \verb|angle| defines the counterclockwise rotation 
    with respect to the (\verb|x|, \verb|y|) point, and \verb|width| defines the width of the restraint. 
    
    \subsection{Springs}
    The available spring, the linear one, is depicted in \cref{fig:springs}. 
    
    \begin{figure}[htbp]
        \centering
        \subfigure[Linear spring\label{fig:spring}]{
        \begin{tikzpicture}
            \node[] (a) at (-2.0, 0.0) {};
            \node[] (b) at (2.0, 0.0) {};
            \linearspring[line width = 1pt](0, 0, 0, 2, 0.5);
        \end{tikzpicture}
        }\qquad \qquad 
        \subfigure[Linear spring without base\label{fig:spring-nobase}]{
        \begin{tikzpicture}
            \node[] (a) at (-2.0, -2.37) {};
            \node[] (b) at (2.0, 0.0) {};
            \linearspringnobase[line width = 1pt](0, 0, 0, 2, 0.5);
        \end{tikzpicture}
        }
        \caption{Springs\label{fig:springs}}
    \end{figure}
    
    The linear spring with base (\cref{fig:spring}) is drawn by the command 
    \begin{verbatim}
        \linearspring[drawing options](x, y, angle, length, width)
    \end{verbatim}
    while the linear spring without base is drawn by the command 
    \begin{verbatim}
        \linearspringnobase[drawing options](x, y, angle, length, width)
    \end{verbatim}
    The field \verb|drawing options| may contains a line width or colour definition, \verb|x| and \verb|y| represent the coordinates of the 
    point at which the spring should connect to the structure (i.e., the top point in \cref{fig:spring}), \verb|angle| defines the counterclockwise rotation 
    with respect to the (\verb|x|, \verb|y|) point, and \verb|length| defines the total length of the spring. 
    
    \subsection{Damping element}
    The available damping elements are depicted in \cref{fig:damping-elements}. 
    
    \begin{figure}[htbp]
        \centering
        \subfigure[Damping element with base\label{fig:damping-element}]{
        \begin{tikzpicture}
            \node[] (a) at (-2.0, 0.0) {};
            \node[] (b) at (2.0, 0.0) {};
            \dampingelement[line width = 1pt](0, 0, 0, 2, 0.5);
        \end{tikzpicture}
        }\qquad \qquad 
        \subfigure[Linear spring without base\label{fig:damping-element-nobase}]{
        \begin{tikzpicture}
            \node[] (a) at (-2.0, -2.37) {};
            \node[] (b) at (2.0, 0.0) {};
            \dampingelementnobase[line width = 1pt](0, 0, 0, 2, 0.5);
        \end{tikzpicture}
        }
        \caption{Damping elements\label{fig:damping-elements}}
    \end{figure}
    
    The damping element with base (\cref{fig:damping-element}) is drawn by the command 
    \begin{verbatim}
        \dampingelement[drawing options](x, y, angle, length, width)
    \end{verbatim}
    while the damping element without base is drawn by the command 
    \begin{verbatim}
        \dampingelementnobase[drawing options](x, y, angle, length, width)
    \end{verbatim}
    The field \verb|drawing options| may contains a line width or colour definition, \verb|x| and \verb|y| represent the coordinates of the 
    point at which the element should connect to the structure (i.e., the top point in \cref{fig:damping-elements}), 
    \verb|angle| defines the counterclockwise rotation 
    with respect to the (\verb|x|, \verb|y|) point, and \verb|length| defines the total length of the element. 
    
    \subsection{Distributed loadings}
    The available distributed loads, the orthogonal linear load linear and the linear load in the y-direction, are depicted in \cref{fig:distributedloads}. 
    
    \begin{figure}[htbp]
        \centering
        \subfigure[Orthogonal linear load]{
            \begin{tikzpicture}
                \orthogonallinearload[line width = 0.5pt](0, 0, 4, 2, 0.5, 1.5, 0.5);
            \end{tikzpicture}
        }\qquad 
        \subfigure[Vertical linear load]{
            \begin{tikzpicture}
                \ylinearload[line width = 0.5pt](0, 0, 4, 2, 0.5, 1.5, 0.5);
            \end{tikzpicture}
        }\\
        \subfigure[Orthogonal linear load with inverted direction]{
            \begin{tikzpicture}
                \orthogonallinearload[line width = 0.5pt](0, 0, 4, 2, 0.5, 1.5, -0.5);
            \end{tikzpicture}
        }\qquad 
        \subfigure[Vertical linear load  with inverted direction]{
            \begin{tikzpicture}
                \ylinearload[line width = 0.5pt](0, 0, 4, 2, 0.5, 1.5, -0.5);
            \end{tikzpicture}
        }
        \caption{Distributed loads\label{fig:distributedloads}}
    \end{figure}
    
    The loadings of \cref{fig:distributedloads} are drawn by the commands
    \begin{verbatim}
        \orthogonallinearload[drawing options](x1, y1, x2, y2, h1, h2, arrows step)
        \ylinearload[drawing options](x1, y1, x2, y2, h1, h2, arrows step)
    \end{verbatim}
    In both the commands, the field \verb|drawing options| may contains a line width or colour definition, \verb|x1|, \verb|y1|, 
    \verb|x2| and \verb|y2| represent the coordinates of end-points of the baseline, \verb|h1| and \verb|h2| represent 
    the height of the load at the end-points of the baseline, and \verb|arrows step| represent the distance between the 
    arrows. The direction of the arrows can be switched by changing the sign of the \verb|arrows step|.
    
    \section{A full example}
    The structural scheme of \cref{fig:introduction-example}, reproduced below with a background grid, has been generated with the following code
    \begin{verbatim}
    \begin{figure}[htbp]
      \centering
      \begin{tikzpicture}
                
        %The bars
        \draw[line width = 1pt] (0.0, 0.0) --++ (0.0, 4.0) 
            --++ (4.0, 0.0) --++ (4.0, -2.0) --++ (2.0, 0.0);
                
        %The restraints
        \fixedendrestraint[line width = 1pt](0.0, 0.0, 0.0, 0.8);
        \pinrestraint[line width = 1pt](0.0, 4.0, -90.0, 0.4);
        \rollerrestraint[line width = 1pt](8.0, 2.0, 0.0, 0.4);
                
        %The linear spring
        \linearspring[line width = 1pt](10.0, 2.0, 180, 0.8, 0.2);
                
        %The loads
        \orthogonallinearload[line width = 0.5pt](0.0, 4.2, 2.5, 4.2, 0.25, 1.0, 0.5);
        \ylinearload[line width = 0.5pt, red](4.0, 4.2, 8.0, 2.2, 0.25, 1.0, 0.5);
                
      \end{tikzpicture}
    \end{figure}
    \end{verbatim}
    
    \begin{figure}[htbp]
        \centering
        \begin{tikzpicture}
            
            %Grid
            \draw[step=0.5cm,gray,very thin] (-1.0, -1) grid (11, 6);
            \draw[step=1.0cm, gray, thin] (-1.0, -1) grid (11, 6);
            
            %The bars
            \draw[line width = 1pt] (0.0, 0.0) --++ (0.0, 4.0) --++ (4.0, 0.0) --++ (4.0, -2.0) --++ (2.0, 0.0);
            
            %The restraints and spring
            \fixedendrestraint[line width = 1pt](0.0, 0.0, 0.0, 0.8);
            \pinrestraint[line width = 1pt](0.0, 4.0, -90.0, 0.4);
            \rollerrestraint[line width = 1pt](8.0, 2.0, 0.0, 0.4);
            \linearspring[line width = 1pt](10.0, 2.0, 180, 0.8, 0.2);
            
            %The loadings
            \orthogonallinearload[line width = 0.5pt](0.0, 4.2, 2.5, 4.2, 0.25, 1.0, 0.5);
            \ylinearload[line width = 0.5pt, red](4.0, 4.2, 8.0, 2.2, 0.5, 1.0, 0.5);
            
        \end{tikzpicture}
    \end{figure}
    
    
\end{document}
