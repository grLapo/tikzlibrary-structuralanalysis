% Copyright 2019 Lapo Gori
% Author: Lapo Gori
%
% This file is part of tikzlibrarystructuralanalysis.
%
% tikzlibrarystructuralanalysis is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 3 of the License, or
% (at your option) any later version.
%
% tikzlibrarystructuralanalysis is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with tikzlibrarystructuralanalysis; if not, see <http:\\www.gnu.org/licenses/>.

%A tikz library with macros for structural engineering drawings.
%Version 0.1.0

\usetikzlibrary{patterns}
\usetikzlibrary{calc}
\usetikzlibrary{intersections}
\usetikzlibrary{arrows.meta}
\usetikzlibrary{bending}
\RequirePackage{xstring}
\RequirePackage{pgfmath}
\RequirePackage{etoolbox}

%%%%%%%%%%%%%%%%%%%%%%%%%
%% Auxiliary functions
%%%%%%%%%%%%%%%%%%%%%%%%%

% This function draws an arc centered at the informed coordinates. Besides the coordinates of 
% its centre, the arc is defined by the initial and final angles and by a radius. 
\def\centerarc[#1](#2)(#3:#4:#5)% Syntax: [draw options] (center) (initial angle:final angle:radius)
{ \draw[#1] ($(#2)+({#5*cos(#3)},{#5*sin(#3)})$) arc (#3:#4:#5); }

% Hatch with arrows. 
% Code adapted from ``https://tex.stackexchange.com/questions/267677/how-i-can-pattern-hatch-an-area-with-arrows-like-picture''
\newcommand\drawarrowshatch{}
\def\drawarrowshatch[#1](#2, #3, #4, #5, #6){
    %\drawarrowshatch[drawing options](top curve, base curve, start x-coord, end x-coord, step)
    \pgfmathsetmacro{\structuralAnalysisSecond}{#4+#6}
    \foreach \Value [count=\xi] in {#4,\structuralAnalysisSecond,...,#5}
    {
        \path[overlay,name path=line\xi] 
        (\Value,100) -- (\Value,-100);
        \path[name intersections={of=#2 and line\xi,by={1\xi}}];
        \path[name intersections={of=#3 and line\xi,by={2\xi}}];
        \ifdim#4pt<\Value pt\relax
        \ifdim\Value pt<#5pt\relax
        \draw[#1, -latex]
        (1\xi) -- (2\xi); 
        \fi\fi
    }
}

%%%%%%%%%%%%%%%%%%%%%%%%%
%% Restraints
%%%%%%%%%%%%%%%%%%%%%%%%%

% Roller restraint.
% This function draws a roller restraint at the informed coordinates, 
% corresponding to the tip of the roller triangle. The function also 
% allows to specify the angle and height of the roller triangle. 
% The other sizes of the restraint are deducted from the informed height.
%TODO adjust the circles radii according to the line width
\newcommand\rollerrestraint{} 
\def\rollerrestraint[#1](#2, #3, #4, #5){
    %\rollerrestraint[drawing options](x, y, angle, height)
    
    %Defining some variables based on the input data
    %Side of the triangle
    \pgfmathsetmacro{\structuralAnalysisLength}{1.1547 * #5}
    %Lenght of the base line
    \pgfmathsetmacro{\structuralAnalysisBaseline}{1.2 * \structuralAnalysisLength}
    %Distance between the triangle and the base line
    \pgfmathsetmacro{\structuralAnalysisDistance}{0.3*#5}
    
    \begin{scope}[rotate around={#4:(#2, #3)}]
        
        %The triangle
        \coordinate (p1) at (#2, #3);
        \coordinate (p2) at (#2 - 0.5 * \structuralAnalysisLength, #3 - #5);
        \coordinate (p3) at (#2 + 0.5 * \structuralAnalysisLength, #3 - #5);
        \draw[#1] (p1.center) -- (p2.center) -- (p3.center) -- cycle;
        %The base line
        \draw[#1] (#2 - 0.5 * \structuralAnalysisBaseline, #3 - #5 - \structuralAnalysisDistance) --++ (\structuralAnalysisBaseline, 0.0);
        
        %The roller circles    
        \draw[#1] (#2 - 0.25 * \structuralAnalysisBaseline, #3 - #5 - 0.5 * \structuralAnalysisDistance) circle (0.5 * \structuralAnalysisDistance cm);
        \draw[#1] (#2 + 0.25 * \structuralAnalysisBaseline, #3 - #5 - 0.5 * \structuralAnalysisDistance) circle (0.5 * \structuralAnalysisDistance cm);
        
    \end{scope}
}

% Pin restraint.
% This function draws a pin restraint at the informed coordinates, 
% corresponding to the tip of the pin triangle. The function also 
% allows to specify the angle and height of the pin triangle. 
% The other sizes of the restraint are deducted from the informed height.
\newcommand\pinrestraint{} 
\def\pinrestraint[#1](#2, #3, #4, #5){
    %\pinrestraint[drawing options](x, y, angle, height)
    
    %Defining some variables based on the input data
    %Side of the triangle
    \pgfmathsetmacro{\structuralAnalysisLength}{1.1547 * #5}
    %Width of the base hatch
    \pgfmathsetmacro{\structuralAnalysisBaseline}{1.0 * \structuralAnalysisLength}
    %Height of the base hatch
    \pgfmathsetmacro{\structuralAnalysisDistance}{0.3*#5}
    
    \begin{scope}[rotate around={#4:(#2, #3)}]
        
        %The triangle
        \coordinate (p1) at (#2, #3);
        \coordinate (p2) at (#2 - 0.5*\structuralAnalysisLength, #3 - #5);
        \coordinate (p3) at (#2 + 0.5*\structuralAnalysisLength, #3 - #5);
        \draw[#1] (p1.center) -- (p2.center) -- (p3.center) -- cycle;
        %The base hatch
        \coordinate (h1) at (#2 - 0.5 * \structuralAnalysisBaseline, #3 - #5);
        \coordinate (h2) at (#2 + 0.5 * \structuralAnalysisBaseline, #3 - #5 - \structuralAnalysisDistance);
        \fill[pattern=north east lines, pattern color = .] (h1.west) rectangle (h2.east);
        
    \end{scope}
}

% Internal pin restraint.
% This function draws a pin restraint without drawing the base.
\newcommand\internalpinrestraint{} 
\def\internalpinrestraint[#1](#2, #3, #4, #5){
    %\internalpinrestraint[drawing options](x, y, angle, height)
    
    %Defining some variables based on the input data
    %Side of the triangle
    \pgfmathsetmacro{\structuralAnalysisLength}{1.1547 * #5}
    %Width of the base hatch
    \pgfmathsetmacro{\structuralAnalysisBaseline}{1.0 * \structuralAnalysisLength}
    %Height of the base hatch
    \pgfmathsetmacro{\structuralAnalysisDistance}{0.3*#5}
    
    \begin{scope}[rotate around={#4:(#2, #3)}]
        
        %The triangle
        \coordinate (p1) at (#2, #3);
        \coordinate (p2) at (#2 - 0.5*\structuralAnalysisLength, #3 - #5);
        \coordinate (p3) at (#2 + 0.5*\structuralAnalysisLength, #3 - #5);
        \draw[#1] (p1.center) -- (p2.center) -- (p3.center) -- cycle;
        
    \end{scope}
}

% Fixed-end restraint.
% This function draws a fixed-end restraint at the informed coordinates, 
% corresponding to the centre of the restraint. The function also 
% allows to specify the angle and width of the base of the restraint. 
% The other sizes of the restraint are deducted from the informed width.
\newcommand\fixedendrestraint{} 
\def\fixedendrestraint[#1](#2, #3, #4, #5){
    %\fixedendrestraint[drawing options](x, y, angle, width)
    
    %Height of the base hatch
    \def\structuralAnalysisDistance{0.3*#5}
    
    \begin{scope}[rotate around={#4:(#2, #3)}]
        
        %The base line
        \draw[#1] (#2 - 0.5 * #5, #3) --++ (#5, 0.0);
        
        %The base hatch
        \coordinate (h1) at (#2 - 0.5 * #5, #3);
        \coordinate (h2) at (#2 + 0.5 * #5, #3 - \structuralAnalysisDistance);
        \fill[pattern=north east lines, pattern color = .] (h1.west) rectangle (h2.east);
        
    \end{scope}
}

%%%%%%%%%%%%%%%%%%%%%%%%%
%% Springs
%%%%%%%%%%%%%%%%%%%%%%%%%

% Linear spring.
% This function draws a linear spring at the informed coordinates, that correspond to 
% the tip of the spring, that is to the point where it should connect to a structure. 
% The function also allows to define the angle, length and width of the spring. 
\newcommand\linearspring{} 
\def\linearspring[#1](#2, #3, #4, #5, #6){
    %\linearspring[drawing options](x, y, angle, length, width)
    
    %The spring step
    \pgfmathsetmacro{\structuralAnalysisSpringStep}{#5/10};
    
    \begin{scope}[rotate around={#4:(#2, #3)}]
        
        %The spring
        \draw[#1] (#2, #3) 
        --++ (0, - #5 * 0.25)
        --++ (- 0.5*#6, - 0.5 * \structuralAnalysisSpringStep)
        --++ (+ #6, - \structuralAnalysisSpringStep)
        --++ (- #6, - \structuralAnalysisSpringStep)
        --++ (+ #6, - \structuralAnalysisSpringStep)
        --++ (- #6, - \structuralAnalysisSpringStep)
        --++ (+ 0.5*#6, - 0.5 * \structuralAnalysisSpringStep)
        --++ (0, - #5 * 0.25);
        
        %The width of the base
        \pgfmathsetmacro{\structuralAnalysisBaseline}{2.2 * #6};
        %Height of the base hatch
        \pgfmathsetmacro{\structuralAnalysisDistance}{0.3*\structuralAnalysisBaseline}
        
        %The base line
        \draw[#1] (#2 - 0.5 * \structuralAnalysisBaseline, #3 - #5) --++ (\structuralAnalysisBaseline, 0.0);
        
        %The base hatch
        \coordinate (h1) at (#2 - 0.5 * \structuralAnalysisBaseline, #3 - #5);
        \coordinate (h2) at (#2 + 0.5 * \structuralAnalysisBaseline, #3 - #5 - \structuralAnalysisDistance);
        \fill[pattern=north east lines, pattern color = .] (h1.west) rectangle (h2.east);
        
    \end{scope}
}

% Linear spring.
% This function draws a linear spring at the informed coordinates, that correspond to 
% the tip of the spring, that is to the point where it should connect to a structure. 
% The function also allows to define the angle, length and width of the spring. Different 
% from the function \linearspring, this one doesn't print the base of the spring. 
\newcommand\linearspringnobase{} 
\def\linearspringnobase[#1](#2, #3, #4, #5, #6){
    %\linearspringnobase[drawing options](x, y, angle, length, width)
    
    %The spring step
    \pgfmathsetmacro{\structuralAnalysisSpringStep}{#5/10};
    
    \begin{scope}[rotate around={#4:(#2, #3)}]
        
        %The spring
        \draw[#1] (#2, #3) 
        --++ (0, - #5 * 0.25)
        --++ (- 0.5*#6, - 0.5 * \structuralAnalysisSpringStep)
        --++ (+ #6, - \structuralAnalysisSpringStep)
        --++ (- #6, - \structuralAnalysisSpringStep)
        --++ (+ #6, - \structuralAnalysisSpringStep)
        --++ (- #6, - \structuralAnalysisSpringStep)
        --++ (+ 0.5*#6, - 0.5 * \structuralAnalysisSpringStep)
        --++ (0, - #5 * 0.25);
        
    \end{scope}
}

%%%%%%%%%%%%%%%%%%%%%%%%%
%% Damping element
%%%%%%%%%%%%%%%%%%%%%%%%%

% Damping element with base.
% It draws a damping element at the informed coordinates, with specified angle and length.

% Damping element.
% This function draws a damping element at the informed coordinates, that correspond to 
% the tip of the damping element, that is to the point where it should connect to a structure. 
% The function also allows to define the angle, length and width of the damping element. 
\newcommand\dampingelement{} 
\def\dampingelement[#1](#2, #3, #4, #5, #6){
    %\dampingelement[drawing options](x, y, angle, length, width)
    
    %The spring step
    \pgfmathsetmacro{\structuralAnalysisSpringStep}{#5/10};
    
    \begin{scope}[rotate around={#4:(#2, #3)}]
        
        %The minor width of the damping element
        \pgfmathsetmacro{\structuralAnalysisWidth}{#6 * 0.8};
        %The height of the damping element
        \pgfmathsetmacro{\structuralAnalysisHeight}{\structuralAnalysisWidth * 0.5};
        %The distance between the damping element parts
        \pgfmathsetmacro{\structuralAnalysisDistance}{0.5 * \structuralAnalysisHeight};
        
        \begin{scope}[rotate around={#4:(#2, #3)}]
            
            \node[] (a) at (#2, #3) {};
            \node[] (b) at (#2, #3 -#5 * 0.5) {};
            \node[] (c) at (#2 - 0.5 * #6, #3 -#5 * 0.5) {};
            \node[] (d) at (#2 - 0.5 * #6, #3 -#5 * 0.5 - \structuralAnalysisHeight) {};        
            \node[] (e) at (#2 + 0.5 * #6, #3 -#5 * 0.5) {};
            \node[] (f) at (#2 + 0.5 * #6, #3 -#5 * 0.5 - \structuralAnalysisHeight) {};
            \node[] (g) at (#2, #3 -#5 * 0.5 - \structuralAnalysisDistance) {};
            \node[] (h) at (#2 - 0.5 * \structuralAnalysisWidth, #3 -#5 * 0.5 - \structuralAnalysisDistance) {};
            \node[] (i) at (#2 + 0.5 * \structuralAnalysisWidth, #3 -#5 * 0.5 - \structuralAnalysisDistance) {};
            \node[] (l) at (#2, #3 -#5) {};
            
            \draw[#1] (a.center) -- (b.center);
            \draw[#1] (b.center) -- (c.center) -- (d.center);
            \draw[#1] (b.center) -- (e.center) -- (f.center);
            \draw[#1] (h.center) -- (i.center);
            \draw[#1] (g.center) -- (l.center);
            
        \end{scope}
        
        %The width of the base
        \pgfmathsetmacro{\structuralAnalysisBaseline}{2.2 * #6};
        %Height of the base hatch
        \pgfmathsetmacro{\structuralAnalysisDistance}{0.3*\structuralAnalysisBaseline}
        
        %The base line
        \draw[#1] (#2 - 0.5 * \structuralAnalysisBaseline, #3 - #5) --++ (\structuralAnalysisBaseline, 0.0);
        
        %The base hatch
        \coordinate (h1) at (#2 - 0.5 * \structuralAnalysisBaseline, #3 - #5);
        \coordinate (h2) at (#2 + 0.5 * \structuralAnalysisBaseline, #3 - #5 - \structuralAnalysisDistance);
        \fill[pattern=north east lines, pattern color = .] (h1.west) rectangle (h2.east);
        
    \end{scope}
}

% Damping element without base.
% This function draws a damping element at the informed coordinates, that correspond to 
% the tip of the damping element, that is to the point where it should connect to a structure. 
% The function also allows to define the angle, length and width of the damping element. Different 
% from the function \dampingelement, this one doesn't print the base of the damping element. 
\newcommand\dampingelementnobase{} 
\def\dampingelementnobase[#1](#2, #3, #4, #5, #6){
    %\dampingelementnobase[drawing options](x, y, angle, length, width)
    
    %The minor width of the damping element
    \pgfmathsetmacro{\structuralAnalysisWidth}{#6 * 0.8};
    %The height of the damping element
    \pgfmathsetmacro{\structuralAnalysisHeight}{\structuralAnalysisWidth * 0.5};
    %The distance between the damping element parts
    \pgfmathsetmacro{\structuralAnalysisDistance}{0.5 * \structuralAnalysisHeight};
    
    \begin{scope}[rotate around={#4:(#2, #3)}]
        
        \node[] (a) at (#2, #3) {};
        \node[] (b) at (#2, #3 -#5 * 0.5) {};
        \node[] (c) at (#2 - 0.5 * #6, #3 -#5 * 0.5) {};
        \node[] (d) at (#2 - 0.5 * #6, #3 -#5 * 0.5 - \structuralAnalysisHeight) {};        
        \node[] (e) at (#2 + 0.5 * #6, #3 -#5 * 0.5) {};
        \node[] (f) at (#2 + 0.5 * #6, #3 -#5 * 0.5 - \structuralAnalysisHeight) {};
        \node[] (g) at (#2, #3 -#5 * 0.5 - \structuralAnalysisDistance) {};
        \node[] (h) at (#2 - 0.5 * \structuralAnalysisWidth, #3 -#5 * 0.5 - \structuralAnalysisDistance) {};
        \node[] (i) at (#2 + 0.5 * \structuralAnalysisWidth, #3 -#5 * 0.5 - \structuralAnalysisDistance) {};
        \node[] (l) at (#2, #3 -#5) {};
        
        \draw[#1] (a.center) -- (b.center);
        \draw[#1] (b.center) -- (c.center) -- (d.center);
        \draw[#1] (b.center) -- (e.center) -- (f.center);
        \draw[#1] (h.center) -- (i.center);
        \draw[#1] (g.center) -- (l.center);
        
    \end{scope}
}


%%%%%%%%%%%%%%%%%%%%%%%%%
%% Loads
%%%%%%%%%%%%%%%%%%%%%%%%%

%It draws a linear load orthogonal to its baseline. The baseline end points are defined by the 
%coordinates x1, y1, x2 and y2, while the height of the load at the end points is defined by h1 and h2.
\newcommand\orthogonallinearload{} 
\def\orthogonallinearload[#1](#2, #3, #4, #5, #6, #7, #8){
    %\orthogonallinearload[drawing options](x1, y1, x2, y2, h1, h2, arrows step)
    
    %Calculating the angle and length of the baseline
    \pgfmathsetmacro{\structuralAnalysisLength}{sqrt((#4 - #2) * (#4 - #2) + (#5 - #3) * (#5 - #3))}
    %     \def\structuralAnalysisLength{sqrt((#4 - #2) * (#4 - #2) + (#5 - #3) * (#5 - #3))}
    \pgfmathsetmacro{\structuralAnalysisTheta}{acos((#4 - #2) / \structuralAnalysisLength)}
    
    \begin{scope}[rotate around={\structuralAnalysisTheta:(#2, #3)}]
        
        %The baseline
        \draw[name path=curve2,#1] (#2, #3) --++ (\structuralAnalysisLength, 0);
        %The topline
        \draw[name path=curve1,#1] (#2, #3 + #6) -- (#2 + \structuralAnalysisLength, #3 + #7);
        
        %The sides
        \draw[#1] (#2, #3) --++ (0, #6);
        \draw[#1] (#2 + \structuralAnalysisLength, #3) --++ (0, #7);
        
        %The arrows
        %         \DrawArrows[0.25]{curve1}{curve2}[#1]{0}{\structuralAnalysisLength} 
        
        \pgfmathsetmacro{\length}{#2 + \structuralAnalysisLength};
        
        \ifdimcomp{#8cm}{>}{0cm}{
            \drawarrowshatch[#1](curve1, curve2, #2, \length, #8);
        }
        {
            \drawarrowshatch[#1](curve2, curve1, #2, \length, -#8);
        }
        
    \end{scope}
    
}

%It draws a linear load, in the Y direction. The baseline end points are defined by the 
%coordinates x1, y1, x2 and y2, while the height of the load at the end points is defined by h1 and h2.
\newcommand\ylinearload{} 
\def\ylinearload[#1](#2, #3, #4, #5, #6, #7, #8){
    %\ylinearload[drawing options](x1, y1, x2, y2, h1, h2, arrows step)
    
    %The baseline
    \draw[name path=curve2,#1] (#2, #3) -- (#4, #5);
    %The topline
    \draw[name path=curve1,#1] (#2, #3 + #6) -- (#4, #5 + #7);
    %The sides
    \draw[#1] (#2, #3) --++ (0, #6);
    \draw[#1] (#4, #5) --++ (0, #7);
    
    \ifdimcomp{#8cm}{>}{0cm}{
        \drawarrowshatch[#1](curve1, curve2, #2, #4, #8);
    }
    {
        \drawarrowshatch[#1](curve2, curve1, #2, #4, -#8);
    }
    
}
