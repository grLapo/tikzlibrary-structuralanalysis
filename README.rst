************************************
TikzLibrary-StructuralAnalysis
************************************

:Date: 28 Mar 2022
:Author: Lapo Gori
:Copyright: 2019 Lapo Gori
:License: GNU General Public License
:Version: 0.1.0

TikzLibrary-StructuralAnalysis
===============================

This is a simple Tikz library for structural analysis drawings. I developed 
this library by collecting a set of macros that I wrote in the last couple of 
years to help me draw some basic structural analysis schemes. 

.. image:: figure.png

----

Getting Started
===============================

Installation
-------------------------------

Clone this repository with: ::

$ git clone https://gitlab.com/grLapo/tikzlibrary-structuralanalysis.git

or directly download the file ``tikzlibrarystructuralanalysis.code.tex`` (you will only need this one 
in order to use the library). Then put the file ``tikzlibrarystructuralanalysis.code.tex`` in the 
directory containing the main source file of your LaTeX project. 

In order to use the commands of the library, simply load the library after 
importing the ``tikz`` package: ::

\usepackage{tikz}
\usetikzlibrary{structuralanalysis}

Examples 
-------------------------------
Besides the library, this repository also contains a LaTeX file (``test.tex``) that illustrates 
how to use the commands of the library. 

----

Notes
===============================
The library is quite simple and, at the moment, only supports two-dimensional drawings. 

Since I'm not an expert in LaTeX programming, the code isn't probably following the best practices, so, any contribution is welcomed! 

----

Known issues
===============================
- When drawing a roller restraint, depending on the specific configuration of height of the restraint and 
  width of the line, the representation of the circles of the roller may not be good. This 
  could be solved by automatically adjusting the radius of the circles according to the 
  informed line width. 
- The hatched area of some of the restraints is obtained by setting a pattern in a ``\fill`` command. 
  In this way, it's not possible to change color of the restraint by passing the color as a 
  drawing option, since this would not change the color of the hatched region. 
